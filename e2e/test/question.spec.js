import { Question } from "../pageObjects/question.js"
import { QuestionList } from "../pageObjects/questionList.js"

const questionText = 'Cuculiforme, malvados?'
const questionErrorText = "El campo pregunta debe estar rellenito."

describe('Questions', () => {
    it('Should be able to send a question', () => {
        let question = new Question()
        let questionList = new QuestionList()

        question
            .typeQuestion(questionText)
            .sendQuestion()

        questionList
            .getQuestionText()
            .contains(questionText)  
    })

    it('Can send a question after an error message', () => {
        let question = new Question()
        let questionList = new QuestionList()

        question
            .sendQuestion()

        question
            .getErrorMessage()
            .contains(questionErrorText)

        question
            .typeQuestion(questionText)
            .sendQuestion()

        questionList
            .getQuestionText()
            .contains(questionText)
    })

})