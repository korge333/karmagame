import { Question } from "../pageObjects/question.js"
import { QuestionList } from "../pageObjects/questionList.js"
import { Answer } from "../pageObjects/answer.js"

const questionText = 'Cuculiforme, malvados?'
const anotherQuestionText = 'Passeriformes'
const answerText = 'First Answer'
const answerText2 = 'Second Answer'

describe('QuestionList', () => {

    it('Should be able to access a completed question and to all its sent answers', () => {
        let question = new Question()
        let questionList = new QuestionList()
        let answer = new Answer()

        question
            .typeQuestion(questionText)
            .sendQuestion()
        
        answer
            .typeAnswer(answerText)
            .sendAnswer()
            .validateAnswer()

        questionList
            .closeQuestion()
            .answersList()
            .should('be.visible')
    })

    it('Should be able to access a completed question and hide all its sent answers', () => {
        let question = new Question()
        let questionList = new QuestionList()
        let answer = new Answer()

        question
            .typeQuestion(questionText)
            .sendQuestion()
        
        answer
            .typeAnswer(answerText)
            .sendAnswer()
            .validateAnswer()

        questionList
            .closeQuestion()
            .toggleAnswersDisplay()
            .answersList()
            .should('not.be.visible')
    })

 
    it('Should be able to send another active question after a previous question is closed', () => {
        let question = new Question()
        let questionList = new QuestionList()

        question
            .typeQuestion(questionText)
            .sendQuestion()

        questionList
            .closeQuestion() 
            .getQuestionText()
            .contains(questionText)
            
        question
            .typeQuestion(anotherQuestionText)
            .sendQuestion()

        questionList
            .closeQuestion()
            .getQuestionText()
            .contains(anotherQuestionText)
    })

    it('Should be able to close the active question with answers pending to validate', () => {
        let question = new Question()
        let questionList = new QuestionList()
        let answer = new Answer()

        question
            .typeQuestion(questionText)
            .sendQuestion()
        
        answer
            .typeAnswer(answerText)
            .sendAnswer()
        
        answer
            .typeAnswer(answerText2)
            .sendAnswer()
        
        questionList
            .closeQuestion()
            .getQuestionText()
            .contains(questionText)
    })

   
})