const KARMA_PATH = "/"
const KARMA_GAME = 'karma-game'
const QUESTION_FORM = 'question-form'
const KARMA_BUTTON = 'karma-button'
const KARMA_INPUT = 'karma-input'
const ERROR_MESSAGE = 'error-message'

export class Question {

    constructor() {
        cy.visit(KARMA_PATH)
    }

    typeQuestion(questionText) {
        this.findQuestionForm()
            .find(KARMA_INPUT).shadow()
            .find('input')
            .type(questionText, { force: true })
        return this
    }

    sendQuestion() {
        this.findQuestionForm()
            .find(KARMA_BUTTON).shadow()
            .find('button')
            .click({force: true}, {multiple: true})
        return this
    }

    getErrorMessage() {
        return this.findQuestionForm()
            .find(ERROR_MESSAGE).shadow()
            .find('h3')
    }

    findQuestionForm() {
        return cy.get(KARMA_GAME).shadow()
            .find(QUESTION_FORM).shadow()
    }
}