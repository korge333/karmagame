const KARMA_PATH = "/"
const KARMA_GAME = 'karma-game'
const QUESTION_LIST = 'question-list'
const ONE_QUESTION = 'one-question'

export class QuestionList {

    constructor() {
        cy.visit(KARMA_PATH)
    }
    
    getQuestionText() {
        return this.findQuestionList()
            .find(ONE_QUESTION).shadow()
            .find('h1')
    }

    closeQuestion() {
        this.findQuestionList()
            .find(ONE_QUESTION).shadow()
            .find('#close-question').shadow()
            .find('button')
            .click({ multiple: true })
        return this
    }

    toggleAnswersDisplay() {
        this.findQuestionList()
            .find(ONE_QUESTION).shadow()
            .find('h1') 
            .click({multiple:true})
        return this
    }

    answersList() {
        return this.findQuestionList()
            .find(ONE_QUESTION).shadow()
            .find('answers-list').shadow()
            .find('component-list').shadow()
            .find('ul')
    }

    findQuestionList() {
        return cy.get(KARMA_GAME).shadow()
            .find(QUESTION_LIST).shadow()
    }
}