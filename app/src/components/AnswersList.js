const answersList = document.createElement('template')
answersList.innerHTML = `
<component-list></component-list>
`

export default class AnswersList extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(answersList.content.cloneNode(true))
        this.answers = []
        this.componentList = this.shadowDOM.querySelector("component-list")
    }

    connectedCallback() {
        document.body.addEventListener("declineAnswer", this.declineAnswer.bind(this))
        document.body.addEventListener("acceptAnswer", this.acceptAnswer.bind(this))
    }

    declineAnswer(event) {
        const answers = this.shadowDOM.querySelectorAll("one-answer")
        answers.forEach(answer => {
            if (event.detail == answer.id) {
                answer.remove()
            }
        })
    }

    acceptAnswer(event) {
        const answers = this.shadowDOM.querySelectorAll("one-answer")
        answers.forEach((answer) => {
            const answerText = answer.shadowDOM.querySelector("p").innerHTML
            
            if (this.isAnswerAccepted(event, answer)) {
                this.answers.push(answerText)
                answer.remove()
            }

            let activeQuestionAnswer = []
            activeQuestionAnswer[0] = event.detail
            activeQuestionAnswer[1] = "validated"

            this.postValidatedAnswer("http://localhost:3000/validatedAnswerState", {activeQuestionAnswer})
        })
        this.validatedAnswers()
        this.answers = []
    }

    isAnswerAccepted(event, answer) {
        return event.detail == answer.id
    }

    postValidatedAnswer(url, answer, id) {
        const postFeatures = {
            method: 'POST',
            body: JSON.stringify(answer,id),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        return fetch(url, postFeatures)
    }
    
    addAnswer(text, id) {
        const answerComponent = document.createElement('one-answer')
        answerComponent.addAnswerText(text)
        const acceptButton = answerComponent.shadowRoot.querySelector('karma-button')
        const declineButton = answerComponent.shadowRoot.querySelectorAll('karma-button')[1]

        acceptButton.id = id
        declineButton.id = id
        answerComponent.id = id
        this.shadowDOM.appendChild(answerComponent)
    }

    validatedAnswers() {
        this.answers.forEach(answer => {
            this.componentList.addElement(answer)
        });
    }
          
}

window.customElements.define("answers-list", AnswersList)