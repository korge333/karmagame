const buttonTemplate = document.createElement('template')
buttonTemplate.innerHTML = `
    <style>
        button{
            background-color: #197B4C;
            color: #F1F3F4;
            border: none;
            border-radius: 5px;
            padding: 7.65px 12px;
            text-align: center;
            font-size: 1.3em;
            font-weight:bold;
            margin: 4px 2px;
            opacity: 0.6;
            transition: 0.3s;
            display: inline-block;
            text-decoration: none;
            cursor: pointer;
            opacity: 1;
        }

        button:hover {
            opacity: 0.8
        }
       
    </style>
    <button></button part="liButton">
`
export default class Button extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(buttonTemplate.content.cloneNode(true))
        this.button = this.shadowDOM.querySelector('button')
    }

    connectedCallback() {
        this.button.addEventListener('click', () => {
            this.eventLaunch()
        })
    }

    static get observedAttributes() {
        return ['name', 'eventName']
    }

    attributeChangedCallback(attribute, oldVal, newVal) {
        if (attribute === "name") {
            this.shadowDOM.querySelector('button').innerHTML = newVal
        }
    }

    get eventName() {
        return this.getAttribute('eventName')
    }

    eventLaunch() {
        this.dispatchEvent(
            new CustomEvent(this.eventName, {
                bubbles: true,
            })
        )
    }
}


window.customElements.define("karma-button", Button)