const answerTemplate = document.createElement('template')
answerTemplate.innerHTML = `
<style>
    .container {
        border-left: 2px;
        max-width: fit-content;
        padding: 10px;
        margin-bottom: 12px;
        background-color: #E29926;
        box-shadow: 0 0.5em 1em -0.125em rgba(10,10,10,0.1), 0 0px 0 1px rgba(10,10,10,0.02);
        color: #4a4a4a;
        max-width: 100%;
        position: relative;
    }

    h5 {
        display: block;
        font-size: 18px;
        margin: 20px;
        color: #252C74;
    }

    #karma-answer {
        margin: 20px;
    }

    #active-question {
        color:white;
        margin: 20px;
    }

</style>
<div class="container">
<h1 id="active-question"></h1>
<h5>Escribe aquí tus respuestas</h5>
<karma-input id="karma-answer" eventName="submitAnswer" placeholder="Nueva respuesta"></karma-input>
<karma-button name="Enviar" id="send-answer" eventName="submitAnswer"></karma-button>
</div>
`

export default class AnswersForm extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({
            mode: 'open'
        })
        this.shadowDOM.appendChild(answerTemplate.content.cloneNode(true))
        this.answerText = ""
        this.url = "http://localhost:3000"
    }

    connectedCallback() {
        this.shadowDOM.addEventListener('updateInputText', this.updateAnswerText.bind(this))
        this.shadowDOM.addEventListener('submitAnswer', this.submitAnswers.bind(this))
        document.addEventListener('sendQuestion', this.displayQuestionOnTop.bind(this))
    }

    submitAnswers() {
        this.removeError()
        if (this.hasAtLeastACharacter()) {
            this.postAnswer(this.url + "/answers", {
                    answer: this.answerText
                })
                .then(() => this.sendAnswerEvent())
                .then(() => this.answerText = "")
        } else {
            this.createError()
        }
    }

    createError() {
        const errorMessage = document.createElement('error-message')
        errorMessage.showErrorMessage('El campo respuesta debe estar rellenito.')
        this.shadowDOM.appendChild(errorMessage)
    }

    removeError() {
        const errorMessage = this.shadowDOM.querySelector('error-message')
        if (errorMessage) {
            errorMessage.remove()
        }
    }

    updateAnswerText(event) {
        this.answerText = event.detail
    }

    sendAnswerEvent() {
        this.dispatchEvent(
            new CustomEvent('sendAnswer', {
                bubbles: true,
                composed: true,
            }))
    }

    hasAtLeastACharacter() {
        const regexp = /\w+/g
        return regexp.test(this.answerText)
    }

    displayQuestionOnTop(event) {
        const activeQuestion = this.shadowDOM.getElementById('active-question')

        this.getActiveQuestion()
            .then(data => {
                activeQuestion.innerHTML = data.question.question
            })
    }

    async getActiveQuestion() {
        try {
            const response = await fetch(this.url + "/active")
            const result = await response.json()
            return result
        } catch (err) {
            console.log('errooooooooooooooooor')
        }
    }

    postAnswer(url, answer) {
        const postFeatures = {
            method: 'POST',
            body: JSON.stringify(answer),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        return fetch(url, postFeatures)
    }

}

window.customElements.define("answers-form", AnswersForm)