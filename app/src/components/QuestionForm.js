import "regenerator-runtime/runtime"
const questionTemplate = document.createElement('template')
questionTemplate.innerHTML = `
<style>
    .container {
        border-left: 2px;
        max-width: fit-content;
        padding: 10px;
        margin-bottom: 12px;
        background-color: #E29926;
        box-shadow: 0 0.5em 1em -0.125em rgba(10,10,10,0.1), 0 0px 0 1px rgba(10,10,10,0.02);
        color: #4a4a4a;
        max-width: 100%;
        position: relative;
    }

    h5 {
        display: block;
        font-size: 18px;
        margin: 20px;
        color: #252C74;
    }

    #question-form {
        margin: 20px;
    }
</style>
<div class="container">
    <h5>Haz tu pregunta</h5>
    <karma-input id="question-form" eventName="submitQuestion"></karma-input>
    <karma-button name="Enviar" id="send-question" eventName="submitQuestion"></karma-button>
</div>
`

export default class QuestionForm extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(questionTemplate.content.cloneNode(true))
        this.questionText = ''
        this.questionForm = document.body.querySelector('question-form')
        this.url = "http://localhost:3000"
    }

    connectedCallback() {
        this.shadowDOM.addEventListener('updateInputText', this.updateQuestionText.bind(this))
        this.shadowDOM.addEventListener('submitQuestion', this.submitQuestion.bind(this))
    }

    updateQuestionText(event) {
        this.questionText = event.detail
    }

    sendQuestionEvent() {
        this.dispatchEvent(
            new CustomEvent('sendQuestion', {
                bubbles: true,
                composed: true, 
            })
        )
    }

    hasAtLeastACharacter() {
        const regexp = /\w+/g
        return regexp.test(this.questionText)
    }
    
    submitQuestion() {
        this.removeError()
        if (this.hasAtLeastACharacter()) {
            this.postQuestion(this.url + "/new", {question:this.questionText})
                .then(() => this.sendQuestionEvent())
            this.questionText = ''
        } else {
            this.createError()
        }
    }

    createError(){
        const errorMessage = document.createElement('error-message')
        errorMessage.showErrorMessage('El campo pregunta debe estar rellenito.')
        this.shadowDOM.appendChild(errorMessage)
    }

    removeError() {
        const errorMessage = this.shadowDOM.querySelector('error-message')
        if(errorMessage){
            errorMessage.remove()
        }
    }

    postQuestion(url, question) {
        const postFeatures = {
            method: 'POST',
            body: JSON.stringify(question),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        return fetch(url, postFeatures)
    }
}

window.customElements.define("question-form", QuestionForm)