const errorTemplate = document.createElement('template')
errorTemplate.innerHTML = `
<style>
    .error {
        color: red;
    }
</style>
<h3 class="error"></h3>
`
export default class ErrorMessage extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(errorTemplate.content.cloneNode(true))
    }

    showErrorMessage(message) {
        this.shadowDOM.querySelector("h3").innerHTML = message
    }
} 

window.customElements.define("error-message", ErrorMessage)