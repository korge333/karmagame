import QuestionForm from '../src/components/QuestionForm'
import ErrorMessage from '../src/components/Error'
import Button from '../src/components/Button'
import Input from '../src/components/Input'

describe("Tests QuestionForm Error messages", function() {
    let componentInput
    let sendButton
    let buttonHtmlElement
    let questionErrorMesssage
    let questionForm
    let dummyTestDouble = () => {}

    beforeEach(() => {
        questionErrorMesssage = 'El campo pregunta debe estar rellenito.'
        questionForm = document.createElement('question-form')
        document.body.appendChild(questionForm)
        sendButton = questionForm.shadowRoot.querySelector("karma-button")
        buttonHtmlElement = sendButton.shadowRoot.querySelector('button')
        componentInput = questionForm.shadowRoot.querySelector('karma-input')
    })

    it ("Displays an error when attempting to send and empty question", function(){
        sendButton.addEventListener('click', () => {dummyTestDouble()})

        buttonHtmlElement.click()
        const componentError = questionForm.shadowRoot.querySelector('error-message')
        const errorMessage = componentError.shadowRoot.querySelector('h3')

        expect(errorMessage.innerHTML).toBe(questionErrorMesssage)
    })

    it('Should appear an error after sending only whitespaces', () => {
        componentInput.innerHTML = '   '
        componentInput.addEventListener('change', (event) => { dummyTestDouble(event) })
        sendButton.addEventListener('click', () => {dummyTestDouble()})

        buttonHtmlElement.click()
        const componentError = questionForm.shadowRoot.querySelector('error-message')
        const errorMessage = componentError.shadowRoot.querySelector('h3')
        
        expect(errorMessage.innerHTML).toBe(questionErrorMesssage)
    })
})
