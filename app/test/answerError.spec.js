import AnswerForm from '../src/components/AnswersForm'
import ErrorMessage from '../src/components/Error'
import Button from '../src/components/Button'
import Input from '../src/components/Input'

describe("Tests AnswersForm error messages", function() {
    let answerComponentInput
    let sendButton
    let buttonHtmlElement
    let errorMesssage
    let answersForm
    let dummyTestDouble = function() {}

    beforeEach(() => {
        errorMesssage = 'El campo respuesta debe estar rellenito.'
        answersForm = document.createElement('answers-form')
        document.body.appendChild(answersForm)
        sendButton = answersForm.shadowRoot.querySelector("#send-answer")
        buttonHtmlElement = sendButton.shadowRoot.querySelector('button')
        answerComponentInput = answersForm.shadowRoot.querySelector('karma-input')
    })

    it ("Displays an error when attempting to send and empty answer", function(){
        sendButton.addEventListener('click', () => {dummyTestDouble()})

        buttonHtmlElement.click()
        const componentError = answersForm.shadowRoot.querySelector('error-message')
        const errorMessage = componentError.shadowRoot.querySelector('h3')

        expect(errorMessage.innerHTML).toBe(errorMesssage)
    })

    it('Should appear an error after sending only whitespaces in the answer form', () => {
        answerComponentInput.innerHTML = '   '
        answerComponentInput.addEventListener('change', (event) => { dummyTestDouble(event) })
        sendButton.addEventListener('click', () => {dummyTestDouble()})

        buttonHtmlElement.click()
        const componentError = answersForm.shadowRoot.querySelector('error-message')
        const errorMessage = componentError.shadowRoot.querySelector('h3')

        expect(errorMessage.innerHTML).toBe(errorMesssage)
    })
})
