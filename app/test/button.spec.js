import Button from '../src/components/Button'

describe("Tests karma-button Component", function() {

    it("Can create a click event", function() {
        const componentButton = document.createElement('karma-button')
        document.body.appendChild(componentButton)

        const spy = jest.fn()
        componentButton.setAttribute('eventName', 'eventClick')
        document.addEventListener('eventClick', (event) => { spy(event) })
        const eventOfButton = new Event('click', { bubbles: true })
        componentButton.shadowRoot.querySelector('button').dispatchEvent(eventOfButton)

        expect(spy).toHaveBeenCalled()
    })

})
