import express from 'express'
import cors from 'cors'
import QuestionCollection from './entity/QuestionCollection.js'
import status from './controller/status.js'

const app = express()
const port = 3000
app.use(express.json())
app.use(cors()) 

const questionCollection = new QuestionCollection()

app.get('/', status)

app.get('/questions', function (request, response) {
  console.log(questionCollection.questions);
  response.json(questionCollection.toJson())
})

app.get('/active', function (request, response) {
  try {
    const activeQuestion = questionCollection.getActiveQuestion()
    response.json(activeQuestion)
  }
  catch (e) {
    return response.status(404).send({message: 'active question not found'})
  }
})

app.get('/closed', function (request, response) {
  const closedQuestions = questionCollection.getClosedQuestions()

  response.json(closedQuestions)
})

app.post("/new", function (request, response){
  let requestedValue = request.body
  questionCollection.addNewQuestion(requestedValue)

  response.json()
})

app.post("/changeStateToClose", function (request, response){
  let requestedValue = request.body
  questionCollection.changeStateToClose(requestedValue.texto)

  response.json()
})

app.post("/answers", function (request, response){
  try {
    const activeQuestion = questionCollection.getActiveQuestion()

    const answerText = request.body.answer
    activeQuestion.addAnswer(answerText)

    response.json()
  }
  catch (e) {
    return response.status(404).send({message: 'active question not found'})
  }
})

app.post("/validatedAnswerState", function (request, response){
  try {
    const activeQuestion = questionCollection.getActiveQuestion()

    let requestedValue = request.body
    let answerId = requestedValue.activeQuestionAnswer[0]
    let status = requestedValue.activeQuestionAnswer[1]

    activeQuestion.changeAnswerState(answerId, status)
    response.json()
  }
  catch (e) {
    return response.status(404).send({message: 'active question not found'})
  }
})

app.listen(port, () => console.log('Server running'))