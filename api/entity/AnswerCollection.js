export default class AnswerCollection {
    constructor() {
        this.answers = []
    }

    addAnswer(text) {
      const oneAnswer = {
          id: this.answers.length,
          status: "nonValidated",
          answer: text 
      }
      return this.answers.push(oneAnswer)
    }
    
    changeAnswerState(id, status) {
      this.answers.forEach(answer => {
        if (answer.id == id) {
          answer.status = status
        }
      })
    }

    toJson() {
        return this.answers
    }
}